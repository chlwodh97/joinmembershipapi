package com.chlwodh97.joinmembershipapi.repository;

import com.chlwodh97.joinmembershipapi.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JoinMembershipRepository extends JpaRepository<Member , Long> {

    long countByUsername(String username);
}


