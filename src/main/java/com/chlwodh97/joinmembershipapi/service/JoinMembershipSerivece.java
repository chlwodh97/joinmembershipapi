package com.chlwodh97.joinmembershipapi.service;


import com.chlwodh97.joinmembershipapi.entity.Member;
import com.chlwodh97.joinmembershipapi.model.JoinMembershipDupCheckResponse;
import com.chlwodh97.joinmembershipapi.model.JoinMembershipRequest;
import com.chlwodh97.joinmembershipapi.repository.JoinMembershipRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class JoinMembershipSerivece {
    private final JoinMembershipRepository joinMembershipRepository;


    /**
     * 프론트엔드한테 부울린타입으로 줄 수 없기 때문에 response 모양을 만들어 컨트롤러 전달
     * @param username
     * @return
     */
    public JoinMembershipDupCheckResponse getMemberIdDupCheck(String username){
        JoinMembershipDupCheckResponse response = new JoinMembershipDupCheckResponse();
        response.setIsNew(isNewUsername(username));

        return response;
    }

    /**
     * 회원을 가입시킨다.
     * @param request 회원가입창에서 고객이 작성한 정보
     * @throws Exception 아이디 중복일 경우 , 비밀번호와 비밀번호 확인이 일치하지 않경우
     */
    public void setJoinMembershipRepository(JoinMembershipRequest request) throws Exception {
        if (!isNewUsername(request.getUsername())) throw new Exception();
        if (request.getPassword().equals(request.getPasswordRe())) throw new Exception();

        Member member = new Member();
        member.setUsername(request.getUsername());
        member.setNickName(request.getNickName());
        member.setPassword(request.getPassword());

        joinMembershipRepository.save(member);

    }



    /**
     * 신규 아이디인지 확인한다.
     *
     *
     * @param username 아이디
     * @return ture 신규아이디 / false 중복아이디
     */
    private boolean isNewUsername(String username){
        long dupCount = joinMembershipRepository.countByUsername(username); // 0

        return dupCount <= 0;
    }
}
