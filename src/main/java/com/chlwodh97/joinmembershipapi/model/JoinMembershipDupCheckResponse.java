package com.chlwodh97.joinmembershipapi.model;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JoinMembershipDupCheckResponse {

    private Boolean isNew;
}
