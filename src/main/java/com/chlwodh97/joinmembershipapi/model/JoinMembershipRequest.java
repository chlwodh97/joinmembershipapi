package com.chlwodh97.joinmembershipapi.model;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JoinMembershipRequest {
    private String nickName;
    private String username;
    private String password;
    private String passwordRe;
}
