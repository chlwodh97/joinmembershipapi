package com.chlwodh97.joinmembershipapi.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Entity
@Setter
@Getter
public class Member {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @Column(nullable = false)
    private String password;

    @Column(nullable = false , unique = true , length = 16)
    private String nickName;

    @Column(nullable = false , length = 20)
    private String username;


    @Column(nullable = false)
    private LocalDateTime dateJoin;
}


